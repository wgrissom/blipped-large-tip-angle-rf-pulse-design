function [g,Npre,Npost] = compute_g_spokes_bipolar(gzarea,c,gzpos,samerewind,gzmax_ref,gxymax,gzslew,gxyslew,upsampfact,dt)

% returns continuous g, NN

gambar = 4257;               % gamma/2pi in Hz/g
gam = gambar*2*pi;         % gamma in radians/g

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
disp('Designing echo-volumar trajectory...');

gztrap = gzpos{1}(:);

diffkx = diff([c(:,1); 0]); %/cm
diffky = diff([c(:,2); 0]);
diffkx(find(diffkx==0))=1e-10;
diffky(find(diffky==0))=1e-10;

gxarea = diffkx/gambar;
gyarea = diffky/gambar;   %G/cm/s

signz = +1;

gx = []; gy = []; gz = [];

gx = [zeros(length(gzpos{1}),1)];
gy = [zeros(length(gzpos{1}),1)];
gz = [signz*gzpos{1}(:)];

for ii = 1:size(c,1)

  gxtrap = sign(gxarea(ii))*dotrap(abs(gxarea(ii)),gxymax,gxyslew,dt);
  gytrap = sign(gyarea(ii))*dotrap(abs(gyarea(ii)),gxymax,gxyslew,dt);

  if ii < size(c,1)
    gztrap = gzpos{ii+1};
  end
  
  signz = -signz;

  if ii==size(c,1)
    PExtindex = length(gz) - length(gxtrap);
    PEytindex = length(gz) - length(gytrap);
  else
    PExtindex = length(gz) - ceil(length(gxtrap)/2);
    PEytindex = length(gz) - ceil(length(gytrap)/2);
  end

  if ii == size(c,1)
    % this is the z rewinder
    if ~samerewind
      traparea = sum(gztrap)*dt; % bc if no ramp sampling, this is ~= gzarea
      gztrap = dotrap(traparea/2,gzmax_ref,gzslew,dt);
    else % if samerewind is true, we use the last gztrap, divided by 2
      gztrap = gztrap/2;
    end
    Npost = length(gztrap);
  end

  gx = [gx;zeros(length(gztrap),1)];
  gy = [gy;zeros(length(gztrap),1)];
  gz = [gz;signz*gztrap(:)];

  gx(PExtindex:PExtindex+length(gxtrap)-1)=gxtrap(:);   
  gy(PEytindex:PEytindex+length(gytrap)-1)=gytrap(:);    
  
  if ii == size(c,1) % add prephasor traps to beginning
    gxpre = sign(-sum(gx)*dt)*dotrap(abs(-sum(gx)*dt),gxymax,gxyslew,dt);
    gypre = sign(-sum(gy)*dt)*dotrap(abs(-sum(gy)*dt),gxymax,gxyslew,dt);
    gzpre = sign(-sum(gz)*dt)*dotrap(abs(sum(gz)*dt),gzmax_ref,gzslew,dt);
    gx = [zeros(length(gzpre),1);gxpre(:);gx(length(gxpre)+1:end)];
    gy = [zeros(length(gzpre),1);gypre(:);gy(length(gypre)+1:end)];
    gz = [gzpre(:);gz];
    Npre = length(gzpre);
  end

end

maxl = max([length(gx) length(gy) length(gz)]);
% add some zeros to make them the same length
gx = [gx; zeros(maxl - length(gx),1)];
gy = [gy; zeros(maxl - length(gy),1)];
gz = [gz; zeros(maxl - length(gz),1)];

%Final g waveforms 
g = [gx gy gz];              % g/cm


