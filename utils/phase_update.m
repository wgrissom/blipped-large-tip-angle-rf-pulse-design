function [pj,psb] = phase_update(d,m,pj,psb,niter)

if isempty(psb)

  % no coupling between spatial locations or between freq
  % bands. just set target phase = excited phase
  pj = angle(m);
  
else % must use opt xfer

  innprod = conj(m).*d;
  
  totmag = sum(abs(innprod),2);
  
  mask = totmag > 0;    
  
  pjm = pj(mask);
  innprodm = innprod(mask,:);
  
  for ii = 1:niter
    
    % update desired phase map
    grad = zeros(size(innprodm));
    grad(:,1) = imag(innprodm(:,1).*exp(1i*pjm));
    for jj = 1:length(psb)
      grad(:,jj+1) = imag(innprodm(:,jj+1).*exp(1i*(pjm+psb(jj))));
    end
    if ~any(grad == 0)
      pjm = pjm - sum(grad,2)./totmag;
    end
    pjm(isnan(pjm)) = 0;
    
    % update band global phase offsets
    for jj = 1:length(psb)
      psb(jj) = psb(jj) - sum(imag(innprodm(:,jj+1).*exp(1i*(pjm+psb(jj)))))/sum(abs(innprodm(:,jj+1)));
    end
    psb(isnan(psb)) = 0;
    
  end % opt xfer iterations

  pj(mask) = pjm;
  
end


