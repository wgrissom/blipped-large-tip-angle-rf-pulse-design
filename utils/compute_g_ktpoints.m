function [g,Npre,Npost,Nadd] = compute_g_ktpoints(c,nrfpts,nblippts,gmax,gslew,dt)

% returns continuous g, NN

gambar = 4257;               % gamma/2pi in Hz/g
gam = gambar*2*pi;         % gamma in radians/g

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
disp('Designing kt-points trajectory...');

ndims = size(c,2);

diffkx = diff([c(:,1); 0]); %/cm
diffky = diff([c(:,2); 0]);
if ndims == 3
  diffkz = diff([c(:,3); 0]);
elseif ndims == 2
  diffkz = zeros(size(diffkx));
end
diffkx(find(diffkx==0))=1e-10;
diffky(find(diffky==0))=1e-10;
diffkz(find(diffkz==0))=1e-10;
  
gxarea = diffkx/gambar;
gyarea = diffky/gambar;   %G/cm/s
gzarea = diffkz/gambar;

gx = []; gy = []; gz = [];

for ii = 1:size(c,1)

  gx = [gx;zeros(nrfpts(ii),1)];
  gy = [gy;zeros(nrfpts(ii),1)];
  gz = [gz;zeros(nrfpts(ii),1)];
  
  gxtrap = sign(gxarea(ii))*dotrap(abs(gxarea(ii)),gmax,gslew,dt).';
  gytrap = sign(gyarea(ii))*dotrap(abs(gyarea(ii)),gmax,gslew,dt).';
  gztrap = sign(gzarea(ii))*dotrap(abs(gzarea(ii)),gmax,gslew,dt).';
  maxtrlen = max([length(gxtrap),length(gytrap),length(gztrap)]);
  
  if maxtrlen > nblippts
    warning('max trap length after subpulse %d is bigger than allowed!',ii);
  end

  gx = [gx;gxtrap;zeros(max([0,nblippts-length(gxtrap)]),1)];
  gy = [gy;gytrap;zeros(max([0,nblippts-length(gytrap)]),1)];
  gz = [gz;gztrap;zeros(max([0,nblippts-length(gztrap)]),1)];
  
  if ii == size(c,1) % if last point

    % add prephasor traps to beginning  
    gxpre = sign(-sum(gx)*dt)*dotrap(abs(-sum(gx)*dt),gmax,gslew,dt).';
    gypre = sign(-sum(gy)*dt)*dotrap(abs(-sum(gy)*dt),gmax,gslew,dt).';
    gzpre = sign(-sum(gz)*dt)*dotrap(abs(-sum(gz)*dt),gmax,gslew,dt).';
    Npre = max([length(gxpre),length(gypre),length(gzpre)]);
    gx = [zeros(max([0,Npre-length(gxpre)]),1);gxpre;gx];
    gy = [zeros(max([0,Npre-length(gypre)]),1);gypre;gy];
    gz = [zeros(max([0,Npre-length(gzpre)]),1);gzpre;gz];
    
    % remove trailing zeros after rewinder
    if nblippts > maxtrlen
      gx = gx(1:end-(nblippts-maxtrlen));
      gy = gy(1:end-(nblippts-maxtrlen));
      gz = gz(1:end-(nblippts-maxtrlen));
      Nadd = 0;
    else
      Nadd = maxtrlen-nblippts;
      maxlen = max([length(gx) length(gy) length(gz)]);
      gx = [gx;zeros(maxlen-length(gx),1)];
      gy = [gy;zeros(maxlen-length(gy),1)];
      gz = [gz;zeros(maxlen-length(gz),1)];
    end
    Npost = maxtrlen;
    
    if Npre > Npost
      % add zeros to the end to make it symmetric
      gx = [gx;zeros(Npre-Npost,1)];
      gy = [gy;zeros(Npre-Npost,1)];
      gz = [gz;zeros(Npre-Npost,1)];
      Nadd = Nadd + Npre - Npost;
      Npost = Npre;
    else
      % add zeros to the beginning to make it symmetric
      gx = [zeros(Npost-Npre,1);gx];
      gy = [zeros(Npost-Npre,1);gy];
      gz = [zeros(Npost-Npre,1);gz];      
      Npre = Npost;
    end
    
  end

end

%Final g waveforms 
g = [gx gy gz];              % g/cm


