__global__
void blochsim_optcont( double *ar,
		       double *ai,
		       double *br,
		       double *bi,
		       double *sensr,
		       double *sensi,
		       double *omdt,
		       double *rfwr,
		       double *rfwi,
		       double *brfr,
		       double *brfi,
		       double *x,
		       double *y,
		       double *z,
		       double *gareax,
		       double *gareay,
		       double *gareaz,
		       int *ntrf,
		       const int ns,
		       const int nr,
		       const int cmplxbrf,
		       const int nc)
{

  /* get spatial index */
  int i = blockIdx.x*blockDim.x + threadIdx.x;

  int j,k; /* counters */
  double sensrfr,sensrfi;
  double art,ait,brt,bit;
  double artt,aitt,brtt;
  double zomr,zomi,zgr,zgi;
  double gtotal;
  double b1r,b1i,b1m;
  double c,s,sr,si;
  int brfc;

  while (i < ns){ /* loop over spatial locs */
    
    /* initialize */
    art = 1;ait = 0;brt = 0;bit = 0;

    zomr = cos(omdt[i]/2);
    zomi = sin(omdt[i]/2);

    brfc = 0; /* reset slice-selective subpulse time counter */

    for(j = 0;j < nr;j++){ /* loop over rungs */

      /* get shimmed b1 for this rung */
      sensrfr = 0;sensrfi = 0;
      for(k = 0;k < nc;k++){ /* loop over tx coils */
	sensrfr += sensr[i+k*ns]*rfwr[j+k*nr] - sensi[i+k*ns]*rfwi[j+k*nr];
	sensrfi += sensi[i+k*ns]*rfwr[j+k*nr] + sensr[i+k*ns]*rfwi[j+k*nr];
      }

      for(k = 0;k < ntrf[j];k++){ /* loop over subpulse time */

	/* weight rf by slice-selective subpulse */
	b1r = brfr[brfc+k]*sensrfr; 
	b1i = brfr[brfc+k]*sensrfi;
	if(cmplxbrf == 1){
	  b1r -= brfi[brfc+k]*sensrfi;
	  b1i += brfi[brfc+k]*sensrfr;
	}
	    
	/* apply RF */
	b1m = sqrt(b1r*b1r + b1i*b1i);
	c = cos(b1m/2);
	s = sin(b1m/2);
	if(b1m > 0){
	  sr = -s*b1i/b1m;
	  si = s*b1r/b1m;
	}else{
	  sr = 0;si = 0;
	}
	artt = art*c - brt*sr - bit*si;
	aitt = ait*c - bit*sr + brt*si;
	brtt = art*sr - ait*si + brt*c;
	bit = art*si + ait*sr + bit*c;
	art = artt;ait = aitt;
	brt = brtt;

	/* apply off resonance */
	artt = art*zomr - ait*zomi;
	ait = ait*zomr + art*zomi;
	art = artt;
	brtt = brt*zomr + bit*zomi;
	bit = bit*zomr - brt*zomi;
	brt = brtt;
	
      }
      brfc += ntrf[j];

      /* apply gradient blip */
      gtotal = x[i]*gareax[j] + y[i]*gareay[j] + z[i]*gareaz[j];
      zgr = cos(gtotal/2);
      zgi = sin(gtotal/2);
      artt = art*zgr - ait*zgi;
      ait = ait*zgr + art*zgi;
      art = artt;
      brtt = brt*zgr + bit*zgi;
      bit = bit*zgr - brt*zgi;
      brt = brtt;

    }
        
    /* copy into result */
    ar[i] = art;
    ai[i] = ait;
    br[i] = brt;
    bi[i] = bit;
 
    i += blockDim.x * gridDim.x;
   
  }

}
