function dout = deriv_optcont_cuda(sens,omdt,rfw,brf,xx,garea,ntbrf,auxa,auxb,af,bf)

persistent gpuderiv
persistent dr di

[Ns,Nd] = size(xx);
[Nr,Nc] = size(rfw);

if isempty(gpuderiv)

  disp 'Initializing GPU derivative calculations';
  % compile with: nvcc -ptx deriv_optcont.cu
  gpuderiv = parallel.gpu.CUDAKernel('deriv_optcont.ptx','deriv_optcont.cu','deriv_optcont');
  tmp = gpuDevice;
  gpuderiv.GridSize = [min(1024,tmp.MaxGridSize(1)) 1]; % min(1024, maxblocks)
  gpuderiv.ThreadBlockSize = [min(128,tmp.MaxThreadsPerBlock) 1 1]; % min(128, maxthreadsperblock)

  dr = gpuArray(zeros(Ns,Nr,Nc));
  di = gpuArray(zeros(Ns,Nr,Nc));
  
end

if prod(size(dr)) ~= Ns*Nr*Nc
  disp('The problem size has changed. Rebuilding GPU arrays.');
  dr = gpuArray(zeros(Ns,Nr,Nc));
  di = gpuArray(zeros(Ns,Nr,Nc));
else
  % zero out arrays 
  dr = 0*dr;
  di = 0*di;
end

if Nd < 3
  xx = [xx zeros(Ns,3-Nd)];
  garea = [garea zeros(Nr,3-Nd)];
end

[dr,di] = feval(gpuderiv,dr,di,real(af),imag(af),real(bf),imag(bf),...
                real(auxa),imag(auxa),real(auxb),imag(auxb),real(sens),imag(sens),...
                omdt,real(rfw),imag(rfw),real(brf),imag(brf),xx(:,1),xx(:,2),xx(:,3),...
                garea(:,1),garea(:,2),garea(:,3),ntbrf,Ns,Nr,~isreal(brf),Nc);

dout = sqz(gather(sum(dr,1) + 1i*sum(di,1)));
