function [a,b] = blochsim_optcont_cuda(sens,omdt,rfw,brf,xx,garea,ntbrf)

persistent gpubloch
persistent ar ai br bi

[Ns,Nd] = size(xx);
[Nr,Nc] = size(rfw);

if isempty(gpubloch)

  disp 'Initializing GPU simulations';
  % compile with: nvcc -ptx blochsim_optcont.cu
  gpubloch = parallel.gpu.CUDAKernel('blochsim_optcont.ptx','blochsim_optcont.cu','blochsim_optcont');
  tmp = gpuDevice;
  gpubloch.GridSize = [min(1024,tmp.MaxGridSize(1)) 1]; % min(1024, maxblocks)
  gpubloch.ThreadBlockSize = [min(128,tmp.MaxThreadsPerBlock) 1 1]; % min(128, maxthreadsperblock)
  
  ar = gpuArray(zeros(Ns,1));
  ai = gpuArray(zeros(Ns,1));
  br = gpuArray(zeros(Ns,1));
  bi = gpuArray(zeros(Ns,1));
  
end

if prod(size(ar)) ~= Ns
  disp('The simulation grid size has changed. Rebuilding GPU arrays.');
  ar = gpuArray(zeros(Ns,1));
  ai = gpuArray(zeros(Ns,1));
  br = gpuArray(zeros(Ns,1));
  bi = gpuArray(zeros(Ns,1));
end

if Nd < 3
  xx = [xx zeros(Ns,3-Nd)];
  garea = [garea zeros(Nr,3-Nd)];
end

[ar,ai,br,bi] = feval(gpubloch,ar,ai,br,bi,real(sens),imag(sens),omdt,real(rfw),imag(rfw),...
                      real(brf),imag(brf),xx(:,1),xx(:,2),xx(:,3),garea(:,1),garea(:,2),garea(:,3),ntbrf,Ns,Nr,~isreal(brf),Nc);

a = gather(ar + 1i*ai);
b = gather(br + 1i*bi);
