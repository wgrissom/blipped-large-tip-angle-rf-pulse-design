function [a,b] = blochsim_optcont(Brf,rfw,sens,garea,xx,omdt)

% Need to make this guy optionally return a,b for gradient update

% inputs
% Brf     {Nrungs} - basis slice-selective waveforms for each rung
% rfw     [Nrungs Nc] - rf weights for each rung and each coil
% sens    [Ns Nc] - tx sensitivities for each coil
% omdt    [Ns 1]  - off-resonance map

Ns = size(xx,1); % Ns: # spatial locs. Nd: # of dimensions
Nrungs = length(Brf);

a = ones(Ns,1);
b = zeros(Ns,1);

% off-resonance
zom = exp(1i/2*omdt);

for jj = 1:Nrungs
  
  sensrf = sens*rfw(jj,:).'; % shim for this rung
  for ii = 1:length(Brf{jj})
    
    b1 = Brf{jj}(ii)*sensrf;
    
    % apply rf
    C = cos(abs(b1)/2);
    S = 1i*exp(1i*angle(b1)).*sin(abs(b1)/2);
    at = a.*C - b.*conj(S);
    bt = a.*S + b.*C;
        
    % apply off-resonance
    a = at.*zom;
    b = bt.*conj(zom);
    
  end
  
  % apply gradient blips
  zg = exp(1i/2*xx*garea(jj,:)');
  a = a.*zg;b = b.*conj(zg);
  
end

