function [rf,garea,a,b,ainit,binit] = blippedltades(rf,garea,probp,algp)

disp('Designing large-tip pulses.')

Ns = size(probp.xx,1); % number of spatial locations
Nb = length(probp.dphib); % number of frequency bands

% string RF subpulses into a single vector for mex/CUDA blochsim
probp.subrfvec = [];
for ii = 1:length(probp.rfsub)
  probp.subrfvec = [probp.subrfvec;probp.rfsub{ii}];
  probp.Ntsubrf(ii) = length(probp.rfsub{ii});
end

% initialize target phase variables and phase-modified patterns
switch algp.phsopt
  case 'joint'
    phsa = zeros(Ns,1);psba = zeros(Nb-1,1);
    phsb = zeros(Ns,1);psbb = zeros(Nb-1,1);
  case {'independent','none'}
    phsa = zeros(Ns,Nb);psba = [];
    phsb = zeros(Ns,Nb);psbb = [];
end
adphs = probp.ad;
bdphs = probp.bd;
    
% Simulate initial pulse
[ainit,binit] = blochsim(rf,garea,probp,algp);
a = ainit;b = binit;
errinit = 1/2*real((abs(ainit(:))-adphs(:))'*(abs(ainit(:))-adphs(:)) + ...
    (abs(binit(:))-bdphs(:))'*(abs(binit(:))-bdphs(:)));

% set large tip lambda to balance error and power
if ~isfield(algp,'lambda')
    lambda = errinit/real(rf(:)'*rf(:));
else
    lambda = algp.lambda;
end

rftime = 0;
gradtime = 0;

cost = [Inf Inf]; % init cost vector
while ((cost(end-1)-cost(end))/cost(end-1) > algp.stopthresh) || (length(cost)-2 <= algp.miniters)
 
    % update target excitation phase
    switch algp.phsopt
        case 'joint'
            if any(abs(probp.ad) > 0) % ad = 0 if 180 so target phase doesnt matter
                [phsa,psba] = phase_update(probp.ad,a,phsa,psba,5);
            end
            [phsb,psbb] = phase_update(probp.bd,b,phsb,psbb,5);
            % apply shifted phase to desired excitation patterns
            adphs(:,1) = probp.ad(:,1).*exp(1i*phsa);
            bdphs(:,1) = probp.bd(:,1).*exp(1i*phsb);
            for ii = 1:Nb-1
                adphs(:,ii+1) = probp.ad(:,ii+1).*exp(1i*(phsa+psba(ii)));
                bdphs(:,ii+1) = probp.bd(:,ii+1).*exp(1i*(phsb+psbb(ii)));
            end
        case 'independent'
            if any(abs(probp.ad) > 0)
                phsa = angle(a);
            end
            phsb = angle(b);
            % apply shifted phase to desired excitation patterns
            adphs = probp.ad.*exp(1i*phsa);
            bdphs = probp.bd.*exp(1i*phsb);
        case 'none'
            if length(cost)-2 == 0
                % use the initial phase as the target phase
                if any(abs(probp.ad) > 0)
                    phsa = angle(a);
                end
                phsb = angle(b);
                % apply shifted phase to desired excitation patterns
                adphs = probp.ad.*exp(1i*phsa);
                bdphs = probp.bd.*exp(1i*phsb);
            end
    end
    
    % update RF subpulse weights
    [rf,a,b] = rfupdate(rf,garea,a,b,adphs,bdphs,lambda,probp,algp);
    
    % update gradient blip areas
    [garea,a,b] = gradupdate(rf,garea,a,b,adphs,bdphs,probp,algp);
    
    % calculate cost
    cost(end+1) = cost_eval(a,b,adphs,bdphs,0,0);
    RFreg = 1/2*lambda*real(rf'*rf);
    
    fprintf('Iter %d. Excitation error: %0.4f. RF power: %0.2f. Peak RF: %0.2f.\n',...
        length(cost)-2,cost(end),(rf'*rf)/2,max(abs(rf)));
    
    % balance RF regularization
    if (rem(length(cost)-2,algp.miniters) == 0) && ...
            ((RFreg > (1+algp.balancefrac)*cost(end)) || ...
             (RFreg < (1-algp.balancefrac)*cost(end)))
        disp('Adjusting RF penalty.');
        lambda = cost(end)/RFreg*lambda;
        cost = [Inf cost(end)+1/2*lambda*real(rf'*rf)];
    else
        cost(end) = cost(end) + RFreg;
    end
    
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Bloch simulation function
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [a,b] = blochsim(rf,garea,probp,algp)

Ns = size(probp.xx,1);
Nb = length(probp.dphib);
Nrungs = size(garea,1);
Nc = size(probp.sens,2);

a = zeros(Ns,Nb);
b = zeros(Ns,Nb);
rfmat = reshape(rf,[Nrungs Nc]);
switch algp.compmethod
    case 'mex'
        for ii = 1:Nb
            [a(:,ii),b(:,ii)] = blochsim_optcont_mex(probp.subrfvec,probp.Ntsubrf,rfmat,...
                complexify(probp.sens),garea,probp.xx,...
                probp.dphi0 + probp.dphib(ii),algp.nthreads);
        end
    case 'gpu'
        for ii = 1:Nb
            [a(:,ii),b(:,ii)] = blochsim_optcont_cuda(probp.sens,...
                probp.dphi0 + probp.dphib(ii),rfmat,probp.subrfvec,probp.xx,garea,probp.Ntsubrf);
        end
    otherwise
        error 'Unrecognized compute method'
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% RF update function
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [rf,a,b] = rfupdate(rf,garea,a,b,ad,bd,lambda,probp,algp)

% get initial cost
cost = cost_eval(a,b,ad,bd,rf,lambda);

fprintf('RF Update - Itr: 0, Cost: %0.4f.\n',cost);

gRF = 0; nn = 1; costOld = Inf;
while nn <= algp.maxcgiters && (costOld - cost)/cost > algp.cgstopthresh
    
    costOld = cost;
    
    % calculate RF weight derivatives
    gRFold = gRF;
    gRF = rfgradcalc(rf,garea,a,b,ad,bd,lambda,probp,algp);
    
    % get the search direction
    if nn == 1
        dir = -gRF;
    else
        betaRF = max(0,real(gRF'*(gRF-gRFold))/real(gRFold'*gRFold));
        dir = -gRF + betaRF*dir;
    end
    
    % calculate the step size
    [alpha,a,b] = rfstepcalc(rf,gRF,dir,garea,a,b,ad,bd,lambda,probp,algp);
    rf = rf + alpha*dir;
    
    % calculate new cost
    cost = cost_eval(a,b,ad,bd,rf,lambda);
    
    fprintf('RF Update - Itr: %d, Err: %0.4f.\n',nn,cost);
    
    nn = nn + 1;
    
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% RF derivative (= gradient) calculation function
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function gRF = rfgradcalc(rf,garea,a,b,ad,bd,lambda,probp,algp)

% get search direction (derivatives)
Nrungs = size(garea,1); % number of spokes/kt-points
Nc = size(probp.sens,2); % number of tx channels
Nb = length(probp.dphib); % number of frequency bands

rfmat = reshape(rf,[Nrungs Nc]);

gRF = 0;
switch algp.compmethod
    case 'mex'
        for ii = 1:Nb
            gRF = gRF + deriv_optcont_mex(probp.subrfvec,probp.Ntsubrf,rfmat,complexify(probp.sens),...
                garea,probp.xx,probp.dphi0+probp.dphib(ii),algp.nthreads,complexify(a(:,ii)-ad(:,ii)),...
                complexify(b(:,ii)-bd(:,ii)),complexify(a(:,ii)),complexify(b(:,ii)));
        end;
    otherwise
        error 'Unrecognized compute method'
end;

% add power penalty derivative
gRF = gRF + lambda*rf;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% RF step size calculation function
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [tl,a,b] = rfstepcalc(rf,gRF,dir,garea,a,b,ad,bd,lambda,probp,algp)

% calculate current cost
cost = cost_eval(a,b,ad,bd,rf,lambda);

% line search to get step
costt = cost;
al = algp.RFal; bl = algp.RFbl; tl = 1/bl;
while (costt > cost + al*tl*real(gRF'*dir)) && tl > algp.RFmintl
    
    % reduce t
    tl = bl*tl;
    
    % get test point
    rft = rf + tl*dir;
    
    % simulate the test point
    [a,b] = blochsim(rft,garea,probp,algp);
        
    % calculate cost of test point
    costt = cost_eval(a,b,ad,bd,rft,lambda);
        
end

if tl == 1/bl % loop was never entered; return zero step
    tl = 0;
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Gradient blip update function
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [garea,a,b] = gradupdate(rf,garea,a,b,ad,bd,probp,algp)

Nc = size(probp.sens,2); % number of tx channels
Ns = size(probp.xx,1); % number of spatial locations
Nb = length(probp.dphib); % number of frequency bands
Nrungs = size(garea,1); % Number of spokes/kT-points

% Calculate a,b for the RF rotations only (one per subpulse; already have a,b for whole pulse)
rfmat = reshape(rf,[Nrungs Nc]);
arf = zeros(Ns,Nb,Nrungs);
brf = zeros(Ns,Nb,Nrungs);
subrfvec = probp.subrfvec;Ntsubrf = probp.Ntsubrf;
probpt = probp;
for ii = 1:Nrungs
    probpt.subrfvec = subrfvec(sum(Ntsubrf(1:ii-1))+1:sum(Ntsubrf(1:ii)));
    probpt.Ntsubrf = Ntsubrf(ii);
    [arf(:,:,ii), brf(:,:,ii)] = blochsim(rfmat(ii,:),0*garea(ii,:),probpt,algp);
end

% get initial cost
cost = cost_eval(a,b,ad,bd,0,0);

fprintf('Grad Update - Itr: 0, Cost: %0.4f.\n',cost);

nn = 1; costOld = Inf; gradg = 0;
while nn <= algp.maxcgiters && (costOld - cost)/cost > algp.cgstopthresh
    
    costOld = cost;
    
    % Calculate derivatives
    gradgold = gradg;
    gradg = gradgradcalc(a,arf,ad,b,brf,bd,probp.xx,garea);
    
    % Get search direction
    if nn == 1
        dir = -gradg;
    else
        betaG = max(0,(gradg(:)'*(gradg(:)-gradgold(:)))/(gradgold(:)'*gradgold(:)));
        dir = -gradg + betaG*dir;
    end
    
    % Get a step size
    [alpha,a,b] = gradstepcalc(dir,gradg,a,b,arf,brf,garea,ad,bd,probp,algp);
    garea = garea + alpha*dir;
    
    % get new cost
    cost = cost_eval(a,b,ad,bd,0,0);
    
    fprintf('Grad Update - Itr: %d, Cost: %0.4f.\n',nn,cost);
    
    nn = nn + 1;
    
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Calculate gradient blip area derivatives
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function gradg = gradgradcalc(a,arf,ad,b,brf,bd,xx,garea)

Nrungs = size(garea,1);
Nd = size(garea,2);
Ns = size(xx,1);
Nb = size(a,2);

resA = a - ad; % residuals
resB = b - bd;

ar = ones(Ns,Nb); br = zeros(Ns,Nb);
af = a; bf = b;

gradg = zeros(Nrungs,Nd);
for jj = Nrungs:-1:1
    
    % current gradient rotation from forward sim.
    zg = repmat(exp(1i/2*xx*garea(jj,:)'),[1 Nb]);
    
    % Calculate the deriv of current gradient rotation
    dA = ar .* af + conj(br) .* bf;
    dB = br .* af - conj(ar) .* bf;
    
    for kk = 1:Nd
        gradg(jj,kk) = 1/2*imag(sum(col(conj(dA).*repmat(xx(:,kk),[1 Nb]).*resA)) + ...
            sum(col(conj(dB).*repmat(xx(:,kk),[1 Nb]).*resB)));
    end
    
    % Strip off current gradient rotation from forward sim.
    af = af.*conj(zg); bf = bf.*zg;
    
    % Add current gradient rotation to backward sim.
    ar = ar.*zg; br = br.*zg;               
                
    % Strip off current rf rotation from forward sim
    aft =  af .* conj(arf(:,:,jj)) + bf .* conj(brf(:,:,jj));
    bft = -af .*      brf(:,:,jj)  + bf .*      arf(:,:,jj);
    af = aft; bf = bft;
    
    % Add current rf rotation to backward sim
    art = ar .* arf(:,:,jj) - conj(br) .* brf(:,:,jj);
    brt = br .* arf(:,:,jj) + conj(ar) .* brf(:,:,jj);
    ar = art; br = brt;
    
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Gradient blip step size calculation function
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [tl,a,b] = gradstepcalc(dir,gradg,a,b,arf,brf,garea,ad,bd,probp,algp)

% calculate current cost
cost = cost_eval(a,b,ad,bd,0,0);

% line search to get step
costt = cost;
al = algp.gradal; bl = algp.gradbl;tl = 1/bl;
while (costt > cost + al*tl*real(gradg(:)'*dir(:))) && tl > algp.gradmintl
    
    % reduce t
    tl = bl*tl;
    
    % get test point
    gareat = garea + tl*dir;
        
    % calculate cost of test point
    [a,b] = garea_to_ab(arf,brf,gareat,probp.xx);
    costt = cost_eval(a,b,ad,bd,0,0);
       
end

if tl == 1/bl % loop was never entered; return zero step
    tl = 0;
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Gradient blip step size calculation function
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function cost = cost_eval(a,b,ad,bd,rf,lambda)

ea = col(a - ad);
eb = col(b - bd);
cost = 1/2*(real(ea'*ea + eb'*eb) + lambda*real(rf'*rf));


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Bloch simulation using pre-calculated RF rotations
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [a,b] = garea_to_ab(arf,brf,garea,xx)

Ns = size(xx,1); % number of spatial locations
Nb = size(arf,2); % number of frequency bands

a = ones(Ns,Nb);
b = zeros(Ns,Nb);

Nrungs = size(arf,3);

for jj = 1:Nrungs
    
    % apply rf rotations
    at = a.*arf(:,:,jj) - b.*conj(brf(:,:,jj));
    bt = a.*brf(:,:,jj) + b.*conj(arf(:,:,jj));
    
    % apply gradient blip rotations
    zg = repmat(exp(1i/2*xx*garea(jj,:)'),[1 Nb]);
    a = at.*zg; b = bt.*conj(zg);
    
end


