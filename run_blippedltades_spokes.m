% Script to design a 2D 180-deg 5-point kT-points pulse
% Copyright Will Grissom and Zhipeng Cao, Vanderbilt University, 2014

clear; clc;

addpath utils

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Load STA pulses and field maps
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Two-channel 3- or 5-spoke anatomical experiment (Fig 5 in article)
%load('blippedltades_3spokes.mat'); % Single frequency 3-spokes in vivo Data
load('blippedltades_5spokes.mat'); % Single frequency 5-spokes in vivo Data

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% scanner and nucleus parameters
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
gmax = 4;                   % gauss/cm, max gradient amplitude
gslewmax = 18000;           % gauss/cm/s, max gradient slew
dt = 6.4e-6;                % seconds, dwell time
gambar = 4257;              % gamma/2pi in Hz/T
gam = gambar*2*pi;          % gamma in radians/g

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% define large-tip target patterns
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
ad = cos(delta_tip*pi/180/2*abs(d));
bd = sin(delta_tip*pi/180/2*abs(d));

% mask target patterns for design
ad = ad(logical(mask(:)),:);
bd = bd(logical(mask(:)),:);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Fill algorithm parameter structures
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
algp.compmethod = 'mex';    % 'mex' or 'CUDA'
algp.nthreads = 4;          % number of mex threads
algp.maxcgiters = 20;       % maximum RF/gradient CG iters
algp.cgstopthresh = 0.0001; % CG consecutive cost stopping threshold
algp.stopthresh = 0.001;    % outer iteration cons. cost stopping threshold
algp.miniters = 10;         % minimum number of outer iterations
algp.phsopt = 'joint';      % mode of phase optimization 
algp.RFal = 0.5;            % RF backtracking line search al
algp.RFbl = 0.5;            % RF backtracking line search bl
algp.RFmintl = 10^-10;      % RF backtracking line search min tl to consider
algp.gradal = 0.5;          % Gradient backtracking line search al
algp.gradbl = 0.5;          % Gradient backtracking line search bl
algp.gradmintl = 10^-10;    % Gradient backtracking line search min tl to consider
algp.balancefrac = 0.25;    % fraction of error-power imbalance we will tolerate
probp.ad = ad;              % target alpha pattern
probp.bd = bd;              % target beta pattern
probp.sens = sensd;         % Tx sensitivity maps
probp.xx = xyzd;            % spatial locs
probp.dphi0 = 2*pi*fmapd*dt;% incremental phase due to off-resonance in one dt
probp.dphib = 2*pi*fb*dt;   % incremental phase due to chemical shift in one dt
probp.rfsub = b1d;          % cell array of rf subpulses

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% run the design
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% calculate areas of STA phase encodes from kpe
garea_sta = diff([kpe;zeros(1,size(kpe,2))],1)*2*pi;
tic
[rf_lta,garea_lta,a_lta,b_lta,a_sta,b_sta] = blippedltades(rf*dt*gam,garea_sta,probp,algp);
toc % report execution time
rf_lta = rf_lta/dt/gam; % convert back to same units as STA pulse
% calculate final phase encode locations from final gradient blip areas
kpelta = -flipud(cumsum(flipud(garea_lta),1))/2/pi;

% embed resulting a, b into full arrays for display
at = zeros(Ns,Nb);
bt = zeros(Ns,Nb);
for ii = 1:Nb
    at(logical(mask(:)),ii) = a_lta(:,ii);
    bt(logical(mask(:)),ii) = b_lta(:,ii);
end
  
at = reshape(at,[dimxyz(:)' Nb]);
bt = reshape(bt,[dimxyz(:)' Nb]);

ref_pattern_lta = bt.^2; % LTA refocusing pattern

at = zeros(Ns,Nb);
bt = zeros(Ns,Nb);
for ii = 1:Nb
    at(logical(mask(:)),ii) = a_sta(:,ii);
    bt(logical(mask(:)),ii) = b_sta(:,ii);
end
  
at = reshape(at,[dimxyz(:)' Nb]);
bt = reshape(bt,[dimxyz(:)' Nb]);

ref_pattern_sta = bt.^2; % STA refocusing pattern
                                                
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% get full LTA RF and gradient waveforms
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
for ii = 1:length(b1d)
  nrfpts(ii) = sum(abs(b1d{ii}) > 0);
end
[glta,Nprelta,Npostlta] = compute_g_spokes_bipolar(gz_area,kpelta,gpos,0,...
    gmax,gmax,gslewmax,gslewmax,1,dt);
[gsta,Npresta,Npoststa] = compute_g_spokes_bipolar(gz_area,kpe,gpos,0,...
    gmax,gmax,gslewmax,gslewmax,1,dt);

% form the full LTA and STA waveforms for display
rf_lta = reshape(rf_lta,[Np Nc]);
rffull_lta = [];
for ii = 1:Np
  rffull_lta = [rffull_lta; b1d{ii}*rf_lta(ii,:)];
end

% add zeros for the spin-echo prewinder
rffull_lta = [zeros(Nprelta,Nc);rffull_lta;zeros(Npostlta,Nc)];

rf = reshape(rf,[Np Nc]);
rffull_sta = [];
for ii = 1:Np
  rffull_sta = [rffull_sta; b1d{ii}*rf(ii,:)];
end

% add zeros for the spin-echo prewinder
rffull_sta = [zeros(Npresta,Nc);rffull_sta;zeros(Npoststa,Nc)];

% final number of samples in pulse
pulse_res = size(rffull_lta,1);

fprintf('Peak STA digital RF magnitude: %0.2f\n',max(abs(rffull_sta(:))));
fprintf('Peak LTA digital RF magnitude: %0.2f\n',max(abs(rffull_lta(:))));
fprintf('Pulse duration: %0.2f ms\n',pulse_res*dt*1000);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% display the pulses and patterns
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
gmax = max(abs(col([glta;gsta])));
gmax = 0.5*ceil(gmax/0.5);
rfmax = max(abs(col([rffull_lta;rffull_sta])));
rfmax = 0.5*ceil(rfmax/0.5);

figure
subplot(211)
t = [0:length(rffull_sta)-1]*dt*1000;
plot(t,real(rffull_sta));
hold on
plot(t,imag(rffull_sta),'g');
plot(t,abs(rffull_sta),'k');
axis([t(1) t(end) -rfmax rfmax]);
legend('real(RF)','imag(RF)','abs(RF)');
xlabel 'milliseconds'
ylabel '/full scale RF'
title 'STA Pulses'

subplot(212)
plot(t,gsta(:,1));
hold on
plot(t,gsta(:,2),'g');
plot(t,gsta(:,3),'r');
axis([t(1) t(end) -gmax gmax]);
legend('g_x(t)','g_y(t)','g_z(t)');
xlabel 'milliseconds'
ylabel 'gauss/cm'

figure
subplot(211)
t = [0:length(rffull_lta)-1]*dt*1000;
plot(t,real(rffull_lta));
hold on
plot(t,imag(rffull_lta),'g');
plot(t,abs(rffull_lta),'k');
axis([t(1) t(end) -rfmax rfmax]);
legend('real(RF)','imag(RF)','abs(RF)');
xlabel 'milliseconds'
ylabel '/full scale RF'
title 'LTA Pulses'

subplot(212)
plot(t,glta(:,1));
hold on
plot(t,glta(:,2),'g');
plot(t,glta(:,3),'r');
axis([t(1) t(end) -gmax gmax]);
legend('g_x(t)','g_y(t)','g_z(t)');
xlabel 'milliseconds'
ylabel 'gauss/cm'

for ii = 1:Nb
    figure
    
    subplot(221);
    imagesc(abs(ref_pattern_sta(:,:,ii)));axis image;colorbar
    title 'STA Refocusing Pattern |\beta^2|'
    subplot(222);
    imagesc(angle(ref_pattern_sta(:,:,ii)));axis image;colormap jet;colorbar
    title 'Phase of STA Refocusing Pattern \angle \beta^2 (Rad)'
    
    subplot(223);
    imagesc(abs(ref_pattern_lta(:,:,ii)));axis image;colorbar
    title 'LTA Refocusing pattern |\beta^2|'
    subplot(224);
    imagesc(angle(ref_pattern_lta(:,:,ii)));axis image;colormap jet;colorbar
    title 'Phase of LTA refocusing pattern \angle \beta^2 (Rad)'
end

if Nb == 2
    figure
    tmp = [col(angle(ref_pattern_sta(:,:,2)./ref_pattern_sta(:,:,1)));...
        col(angle(ref_pattern_lta(:,:,2)./ref_pattern_lta(:,:,1)))];
    subplot(121);
    imagesc(angle(ref_pattern_sta(:,:,2)./ref_pattern_sta(:,:,1)),[min(tmp) max(tmp)]);
    axis image;colorbar
    title 'STA Band Phase Difference'
    subplot(122);
    imagesc(angle(ref_pattern_lta(:,:,2)./ref_pattern_lta(:,:,1)),[min(tmp) max(tmp)]);
    axis image;colorbar
    title 'LTA Band Phase Difference'
end





